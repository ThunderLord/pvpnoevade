
package me.soliddanii;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import org.bukkit.Bukkit;
import org.bukkit.block.Sign;


public class Config {
    public static int pvpDuration;
    public static boolean addAlways;
    public static boolean punish;
    public static String quitMessage;
    
    public static String configFile = System.getProperty("user.dir")+"/plugins/PvpNoEvade/config.cfg";
    
    
    public static void loadConfig(){
       
       Properties props = new Properties();
       InputStream is = null;
 
        // First try loading from the current directory
        try {
            File f = new File(configFile);
            is = new FileInputStream( f );
            props.load( is );
   
            pvpDuration = Integer.valueOf(props.getProperty("pvpDuration").toString()); 
            addAlways = getBoolean(props,"addAlways"); 
            punish = getBoolean(props,"punish"); 
            quitMessage = props.getProperty("quitMessage").toString(); 
            System.out.println("[PvpNoEvade] Config Loaded Succesfully");
            
            is.close();
            }catch ( Exception e ) {  }  
   }
    
    
    public static void InputStreamAFile(InputStream entrada, String Salida){
        try{
              File f=new File(Salida);//Aqui le dan el nombre y/o con la ruta del archivo salida
              OutputStream salida=new FileOutputStream(f);
                byte[] buf =new byte[1024];//Actualizado me olvide del 1024
                int len;
              while((len=entrada.read(buf))>0){
                  salida.write(buf,0,len);
              }
              salida.close();
              entrada.close();
        }catch(IOException e){
              System.out.println("Se produjo el error : "+e.toString());
        }
    
    }
    
    private static boolean getBoolean(Properties props, String key){
        if(props.getProperty(key).toString().equals("true")){
            return true;
        }else{
            return false;
        }
    }
    
}
