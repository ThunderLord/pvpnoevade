/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.soliddanii;

import java.io.FileNotFoundException;
import java.io.IOException;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;


public class QuitListener implements Listener{
    
    
    public QuitListener() {
            Bukkit.getServer().getPluginManager().registerEvents(this, PvpNoEvade.plugin);
        }
    
     @EventHandler(priority = EventPriority.NORMAL)
        public void onquit(PlayerQuitEvent e) throws FileNotFoundException, IOException{
            final Player p = e.getPlayer();
            if(p!=null){
                 //check if disconnected
                if(PvpNoEvade.listaPvP.contains(p) ){
                    
                    String reason = "Cant Get Reason";
                    if(Config.quitMessage.contains("{REASON}")){
                        //get disconnect reason from log
                        //reason = FileReader.tail(Main.logFile, 1);
                        //reason = reason.substring(reason.indexOf("[INFO]"+p.getDisplayName()+" lost connection")+(p.getDisplayName().length()+22)); 
                        reason = FileReader.tail(PvpNoEvade.logFile, 2);
                        reason = reason.substring(reason.length()-140);
                                   
                        //reason = FileReader.reader(Main.logFile.toString(), 1).substring(26);
                    }else{
                        reason = e.getQuitMessage().toString();
                    }

                    //set string message
                    String message = Config.quitMessage.replace("{PLAYER}", p.getDisplayName());
                    message = message.replace("{REASON}", reason);
                    System.out.println("[PvpNoEvade] "+message);
                    Bukkit.getServer().broadcastMessage(ChatColor.GREEN+"[PvpNoEvade]"+ChatColor.AQUA+message);
                    PvpNoEvade.listaPvP.remove(p);
                    
                    //IF CONFIG ESPRCIFIED PUNISH: DROP ALL INVENTORY TO GROUND
                    if(Config.punish==true){
                        Location loc = p.getLocation().clone();
                        Inventory inv = p.getInventory();
                        for (ItemStack item : inv.getContents()) {
                            if (item != null) {
                            loc.getWorld().dropItemNaturally(loc, item.clone());
                            }
                        }
                        inv.clear();
                        }
                }   
            }else{
                System.out.println("[PvpNoEvade][Error] Null Player onDisconnect");
            }
            
        }
}
