
package me.soliddanii;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Daniel
 */
public class FileReader {
    
    
    public static String tail( File file, int lines) {
        java.io.RandomAccessFile fileHandler = null; 
        try {
            fileHandler =   new java.io.RandomAccessFile( file, "r" );
            long fileLength = file.length() - 1;
            StringBuilder sb = new StringBuilder();
            int line = 0;

            for(long filePointer = fileLength; filePointer != -1; filePointer--){
                fileHandler.seek( filePointer );
                int readByte = fileHandler.readByte();

                if( readByte == 0xA ) {
                     if (line == lines) {
                        if (filePointer == fileLength) {
                            continue;
                        } else {
                            break;
                        }
                    }
                } else if( readByte == 0xD ) {
                    line = line + 1;
                    if (line == lines) {
                         if (filePointer == fileLength - 1) {
                             continue;
                        } else {
                             break;
                        }
                     }
                }
                sb.append( ( char ) readByte );
            }

            sb.deleteCharAt(sb.length()-1);
            String lastLine = sb.reverse().toString();
            return lastLine;
        } catch( java.io.FileNotFoundException e ) {
            System.err.println(e);
            return null;
        } catch( java.io.IOException e ) {
            System.err.println(e);
            return null;
        }finally {
            try { fileHandler.close(); } catch (IOException ex) { }
        }
    }
    
    public static String reader(String direccion, int lines) throws FileNotFoundException, IOException{
        FileInputStream file = new FileInputStream(direccion);
        BufferedReader br = new BufferedReader(new InputStreamReader(file));
 
        String strLine = null, tmp;
        while ((tmp = br.readLine()) != null) {
            strLine = tmp;
         }
        String lastLine = strLine;
        file.close();
        
        return lastLine;
    }
}
