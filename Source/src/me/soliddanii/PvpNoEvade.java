package me.soliddanii;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;


public class PvpNoEvade extends JavaPlugin {

        public static ArrayList<Player> listaPvP = new ArrayList<Player>();
        public static File logFile = new File(System.getProperty("user.dir")+"/server.log");
        public static PvpNoEvade plugin;
        
	@Override
	public void onDisable() {
            log("Plugin Succesfully Disabled",0);
	}
	@Override
	public void onEnable() {
            plugin = this;
            new DamageListener();
            new QuitListener();
            
            //Inicializar configuracion
            if(new File(System.getProperty("user.dir")+"/plugins/PvpNoEvade/config.cfg").exists()){
                Config.loadConfig();
            }else{
                new File(System.getProperty("user.dir")+"/plugins/PvpNoEvade").mkdirs(); 
                //Extraer configuracion por defecto y cargarla
                extract();
                Config.loadConfig();
            }
            
            log("Plugin by soliddanii succesfully enabled",2);   
	}
        
        
        private void log(String text, int type){
            
            if(type==0){ //tipo 0, normal
                getLogger().info("[PvpNoEvade] "+text);
            }else if (type==1){ //tipo 1, error
                getLogger().info("[PvpNoEvade][Error] "+text);
            }else{ //tipo 2, sin cabecera
                getLogger().info(text);
            }
            
        }
        
        @Override
        public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
            String commandName = cmd.getName().toLowerCase();
            if(commandName.equals("pvpl")){
                if(args.length==0){
                    sender.sendMessage(ChatColor.GREEN+"[PvpNoEvade] "+ChatColor.AQUA+"Numero de argumentos invalido: /pvpl [clear][size][check][cfg]");
                }else{
                    if (args[0].equals("size")){
                        if (!listaPvP.isEmpty()){
                            sender.sendMessage(ChatColor.GREEN+"[PvpNoEvade] "+ChatColor.AQUA+"Tamaño de la lista PVP: "+listaPvP.size());
                        }else{
                             sender.sendMessage(ChatColor.GREEN+"[PvpNoEvade] "+ChatColor.AQUA+"Tamaño de la lista PVP: "+ChatColor.RED+"vacia");
                        }
                    }
                    if(args[0].equals("clear")){
                        if (!listaPvP.isEmpty()){
                            listaPvP.clear();
                            sender.sendMessage(ChatColor.GREEN+"[PvpNoEvade] "+ChatColor.AQUA+"Lista PvP  limpiada correctamente");
                        }else{
                            sender.sendMessage(ChatColor.GREEN+"[PvpNoEvade] "+ChatColor.AQUA+"La lista PVP ya estaba vacia.");
                        }
                    }
                
                    if(args[0].equals("check")){
                        if (args.length >= 2){
                            if(listaPvP.contains(Bukkit.getServer().getPlayer(args[1]) )){
                                sender.sendMessage(ChatColor.GREEN+"[PvpNoEvade] "+ChatColor.AQUA+"El jugador "+args[1]+" esta en la lista PVP");
                            }else{
                                sender.sendMessage(ChatColor.GREEN+"[PvpNoEvade] "+ChatColor.AQUA+"El jugador "+args[1]+" NO esta en la lista PVP");
                            }
                        }else{
                             sender.sendMessage(ChatColor.GREEN+"[PvpNoEvade] "+ChatColor.AQUA+"Numero de argumentos invalido: /pvpl check <nombre>");
                        }
                    }
                    if(args[0].equals("cfg")){
                            
                            sender.sendMessage(ChatColor.GREEN+"[PvpNoEvade] "+ChatColor.AQUA+"Mensaje de desconexion pvp: "+ChatColor.BOLD+Config.quitMessage);
                            sender.sendMessage(ChatColor.GREEN+"[PvpNoEvade] "+ChatColor.AQUA+"AddAlways: "+ChatColor.BOLD+Config.addAlways);
                            sender.sendMessage(ChatColor.GREEN+"[PvpNoEvade] "+ChatColor.AQUA+"Castigar Jugadores: "+ChatColor.BOLD+Config.punish);
                            sender.sendMessage(ChatColor.GREEN+"[PvpNoEvade] "+ChatColor.AQUA+"Tiempo Estado PVP: "+ChatColor.BOLD+Config.pvpDuration);
                            
                    }
                }  
            }
       
        return true;
        }
	
        private void extract(){
        
        InputStream is = getClass().getResourceAsStream("config.cfg");
        Config.InputStreamAFile(is, Config.configFile.toString());
                
        
    }
       
}
