package me.soliddanii;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class DamageListener implements Listener {
    
        
        public DamageListener() {
            Bukkit.getServer().getPluginManager().registerEvents(this, PvpNoEvade.plugin);
        }

	@EventHandler(priority = EventPriority.NORMAL)
	public  void onPvP(EntityDamageByEntityEvent event)  {
            
             Entity ent = event.getEntity();
             Entity ent1 = event.getDamager();
             if(ent instanceof Player && ent1 instanceof Player){
                 Player victim =(Player) ent;
                 Player damager = (Player) ent1;               
                 checkPvP(damager,victim);
            }
           
        
            
	}
        
       private void checkPvP(final Player damager, final Player victim){
           //Get pvp status duration ticks
           long pvp = Config.pvpDuration*20L;
           
           //Si no esta en la lista, o se especifica MeterSiempre
           if(!PvpNoEvade.listaPvP.contains(victim) || Config.addAlways == true){
               PvpNoEvade.listaPvP.add(victim);
           }
           if(!PvpNoEvade.listaPvP.contains(damager) || Config.addAlways == true){
               PvpNoEvade.listaPvP.add(damager);
           }
                    PvpNoEvade.plugin.getServer().getScheduler().runTaskLaterAsynchronously(PvpNoEvade.plugin, new Runnable() {
                    public void run() {
                        //Borrarlos de la lista si aparecian tras esperar 10 segundos.
                        if(PvpNoEvade.listaPvP.contains(victim)){PvpNoEvade.listaPvP.remove(victim);}
                        if(PvpNoEvade.listaPvP.contains(damager)){PvpNoEvade.listaPvP.remove(damager);}
                    }
                }, pvp);// 60 L == 3 sec, 20 ticks == 1 sec
       }
        
        
       private void log(String text, int type){
            
            if(type==0){ //tipo 0, normal
                PvpNoEvade.plugin.getLogger().info("[PvpNoEvade] "+text);
            }else if (type==1){ //tipo 1, error
                PvpNoEvade.plugin.getLogger().info("[PvpNoEvade][Error] "+text);
            }else{ //tipo 2, sin cabecera
                PvpNoEvade.plugin.getLogger().info(text);
            }
            
        }
        
}
